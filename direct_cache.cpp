#include <cmath>
#include <cstdint>
#include <iostream>
#include <vector>

struct CacheBlock {
    bool valid;
    std::uint64_t tag;

    CacheBlock() {
        this->valid = false;
        this->tag = -1;
    }
};

enum class CacheResult { Hit, Miss };

class Cache {
  public:
    Cache(std::size_t index_bits) { blocks.resize(std::pow(2, index_bits)); }

    CacheResult access_at(std::uint32_t address) {
        std::uint32_t index = address % this->blocks.size();
        std::uint32_t tag = address / this->blocks.size();

        CacheBlock& block = blocks[index];

        if (block.valid && block.tag == tag) {
            return CacheResult::Hit;
        } else {
            block.valid = true;
            block.tag = tag;

            return CacheResult::Miss;
        }
    }

  private:
    std::vector<CacheBlock> blocks;
};

int main() {
    Cache cache(10);
    std::vector<std::uint32_t> addresses;

    std::uint32_t addr;
    while (!std::cin.eof() && std::cin >> std::hex >> addr) {
        addresses.push_back(addr);
    }

    for (auto address : addresses) {
        if (cache.access_at(address) == CacheResult::Hit) {
            std::cout << "0x" << std::hex << address << ": "
                      << "cache hit!" << std::endl;
        } else {
            std::cout << "0x" << std::hex << address << ": "
                      << "cache miss!" << std::endl;
        }
    }

    return 0;
}
