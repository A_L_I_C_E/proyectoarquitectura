> Estudiantes:
> Fabricio Parra 30.837.006
> Elio Guarate 28.182.524

**Programas de simulación de caché**

Este repositorio contiene tres programas de simulación de caché:

## `direct_cache.cpp`

Este programa implementa una caché mapeada directamente. Una caché mapeada directamente es
un tipo de caché donde cada dirección de memoria se mapea a un bloque de caché único. Esto
significa que no puede haber colisiones de caché, que son situaciones en las que dos
direcciones de memoria diferentes se mapean al mismo bloque de caché.

El programa `direct_cache.cpp` toma como entrada un recuento de bits de índice. El recuento
de bits de índice determina el tamaño de la caché. El programa luego lee direcciones de
memoria de la entrada estándar y simula accesos a la caché. Para cada dirección de
memoria, el programa imprime si el acceso fue un acierto o una falta.

## `associative_cache.cpp`

Este programa implementa una caché totalmente asociativa. Una caché totalmente asociativa
es un tipo de caché donde cualquier dirección de memoria puede mapearse a cualquier
bloque de caché. Esto significa que puede haber colisiones de caché, pero la caché puede
contener más datos que una caché mapeada directamente del mismo tamaño.

El programa `associative_cache.cpp` toma como entrada un recuento de bits de índice. El recuento
de bits de índice determina el tamaño de la caché.

## `associative_set_cache.cpp`

Este programa implementa una caché asociativa por conjuntos. Una caché asociativa por
conjuntos es un tipo de caché que es un compromiso entre una caché mapeada directamente
y una caché totalmente asociativa. Una caché asociativa por conjuntos tiene múltiples
conjuntos de caché, cada uno de los cuales es una caché mapeada directamente. Esto significa
que puede haber algunas colisiones de caché, pero la caché aún puede contener más datos que
una caché mapeada directamente del mismo tamaño.

El programa `associative_set_cache.cpp` toma como entrada un tamaño y un recuento de
bits de índice. El tamaño determina el número de conjuntos de caché, y el recuento de
bits de índice determina el tamaño de cada conjunto de caché.

## Formato de entrada

El formato de entrada para los tres programas es una secuencia de direcciones de
memoria, cada una dada en notación hexadecimal. Las direcciones están separadas por espacios
o nuevas líneas.

## Formato de salida

El formato de salida para los tres programas es una secuencia de líneas, cada una de las
cuales corresponde a una dirección de memoria. Para cada dirección de memoria, la línea de
salida consta de la dirección de memoria, dos puntos, un espacio y uno de los siguientes:
"cache hit!" o "cache miss!".

**Ejemplo de entrada y salida**

```
0x0001
0x01010
0x10AB
```

```
0x0001: cache hit!
0x01010: cache hit!
0x10AB: cache miss!
```

En este ejemplo, la primera y segunda direcciones de memoria se encuentran en la caché, por
lo que se registran como aciertos. La tercera dirección de memoria no se encuentra en la
caché, por lo que se registra como una falta.
