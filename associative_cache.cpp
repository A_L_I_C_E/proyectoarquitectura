#include <cmath>
#include <cstdint>
#include <iostream>
#include <optional>
#include <vector>

struct CacheBlock {
    bool valid;
    std::uint32_t tag;
    std::uint32_t last_used;

    CacheBlock() {
        this->valid = false;
        this->tag = -1;
        this->last_used = 0;
    }
};

enum class CacheResult { Hit, Miss };

class Cache {
  public:
    Cache(std::size_t index_bits) {
        this->tick = 0;
        blocks.resize(std::pow(2, index_bits));
    }

    CacheResult access_at(std::uint32_t address) {
        this->tick++;

        std::uint32_t tag = address / this->blocks.size();
        std::optional<CacheBlock> last_used_block;

        for (auto& block : this->blocks) {
            if (block.valid && block.tag == tag) {
                block.last_used = this->tick;
                return CacheResult::Hit;
            }

            if (!last_used_block.has_value() ||
                block.last_used < last_used_block.value().last_used) {
                last_used_block = block;
            }
        }

        if (last_used_block.has_value()) {
            auto& block = last_used_block.value();

            block.valid = true;
            block.tag = tag;
            block.last_used = tick;
        }

        return CacheResult::Miss;
    }

  private:
    std::uint32_t tick;
    std::vector<CacheBlock> blocks;
};

int main() {
    Cache cache(10);
    std::vector<std::uint32_t> addresses;

    std::uint32_t addr;
    while (!std::cin.eof() && std::cin >> std::hex >> addr) {
        addresses.push_back(addr);
    }

    for (auto address : addresses) {
        if (cache.access_at(address) == CacheResult::Hit) {
            std::cout << "0x" << std::hex << address << ": "
                      << "cache hit!" << std::endl;
        } else {
            std::cout << "0x" << std::hex << address << ": "
                      << "cache miss!" << std::endl;
        }
    }

    return 0;
}
